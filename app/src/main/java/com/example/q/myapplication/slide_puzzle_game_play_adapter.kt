package com.example.q.myapplication

import android.content.Context
import android.graphics.Bitmap
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView

class slide_puzzle_game_play_adapter(val context: Context, val Photodatas: Array<slide_puzzle_game_play.picturePiece?>) : BaseAdapter() {

    var mContext : Context? = context
    var mPhotodatas : Array<slide_puzzle_game_play.picturePiece?> = Photodatas

    override fun getCount(): Int {
        return mPhotodatas!!.size
    }

    override fun getItem(position : Int): Bitmap? {
        return mPhotodatas!![position]!!.picture
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var imageView : ImageView
        imageView = if(convertView == null){
            ImageView(mContext)
        }else{
            convertView as ImageView
        }
        imageView!!.setImageBitmap(mPhotodatas!![position]!!.picture)
        return imageView
    }

}
