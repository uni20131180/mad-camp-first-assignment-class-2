package com.example.q.myapplication
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView

class dodge_game_scoreboard : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dodge_game_scoreboard)

        var dbLoader1 = GameScoreDatabaseUsgae("dodge", 0.0, 0, this)
        var cursor1 = dbLoader1.GetAllData()

        var resultString = ""

        resultString += "Life Time \n"
        if (cursor1!!.moveToFirst()) {
            do {
                resultString += cursor1.getString(1) + " : " + cursor1.getDouble(2) + "\n"
            } while (cursor1!!.moveToNext())
        }

        var textV = findViewById<TextView>(R.id.dodgescoreboard)
        textV.setText(resultString)
    }
}