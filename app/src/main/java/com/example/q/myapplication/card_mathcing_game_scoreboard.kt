package com.example.q.myapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView

class card_matching_game_scoreboard : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.slide_puzzle_scoreboard)
        var dbLoader = GameScoreDatabaseUsgae("card_matching_game", 0.0, 1, this)
        var cursor = dbLoader.GetAllData()
        var resultString = ""
        resultString += "Ranking\n"
        if (cursor!!.moveToFirst()) {
            do {
                resultString += cursor.getString(1) + " : " + cursor.getDouble(2) + "\n"
            } while (cursor!!.moveToNext())
        }
        var textV = findViewById<TextView>(R.id.slidepuzzlescore)
        textV.setText(resultString)
    }

}