package com.example.q.myapplication
import android.graphics.Bitmap

class PhoneGallery(val mediaID : String, val mediaData : String, val mediaDisplay: String, val mediaSize: String, val bitmap: Bitmap)