package com.example.q.myapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView

class address_matching_game_scoreboard : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.address_matching_game_scoreboard)

        var dbLoader1 = GameScoreDatabaseUsgae("addressmatching", 0.0, 0, this)
        var cursor1 = dbLoader1.GetAllData()

        var resultString = ""

        resultString += "Correct Problem \n"
        if (cursor1!!.moveToFirst()) {
            do {
                resultString += cursor1.getString(1) + " : " + cursor1.getDouble(2) + "\n"
            } while (cursor1!!.moveToNext())
        }

        var textV = findViewById<TextView>(R.id.addressmatchinggamescoreboard)
        textV.setText(resultString)
    }
}