package com.example.q.myapplication

import android.content.Intent
import android.graphics.Point
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button

class dodge_game_menu : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_menu)
        setTitle("닷지")

        val button1 = findViewById<Button>(R.id.gameplay)
        val button2 = findViewById<Button>(R.id.gamescore)
        val button3 = findViewById<Button>(R.id.gamehelp)

        button1.setOnClickListener {
            val dodgeActivity = Intent(this, dodge_game_play::class.java)
            var a= window.windowManager.defaultDisplay
            var b = Point()
            a.getSize(b)
            dodgeActivity.putExtra("width", b.x)
            dodgeActivity.putExtra("height", b.y)
            startActivity(dodgeActivity)
        }
        button2.setOnClickListener {
            // 스코어 액티비티로 넘긴다.
            // 게임 액티비티 내에서 최고 스코어만 저장한 뒤, 불러온다.
            val dodgeActivity = Intent(this, dodge_game_scoreboard::class.java)
            startActivity(dodgeActivity)
        }
        button3.setOnClickListener {
            // 게임 플레이 방법을 도움말에 대사로 첨부한다.
            // 여기다가 Fragment 를 사용한다.
        }

    }
}