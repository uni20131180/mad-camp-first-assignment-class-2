package com.example.q.myapplication

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import java.util.*
import android.view.MotionEvent
import android.view.View.OnTouchListener



class Snake_Game_Play : AppCompatActivity () {

    var view : View? = null
    var snake : Snake_Game_Body? = null
    var timer2 : Timer? = null
    var x_start = 0.0F
    var y_start = 0.0F
    var x_end = 0.0F
    var y_end = 0.0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.snake_game_play)
        setTitle("스네이크 (세베루스 아님)")
        view = ImageView(this)
        setContentView(view)

        val task2 = object : TimerTask() {
            override fun run() {
                if(snake!!.auto_move_stack > snake!!.auto_move){
                    snake!!.Move()
                    snake!!.auto_move_stack = 0
                }
                if(snake!!.timeFast_stack > snake!!.timeFast){
                    snake!!.auto_move -= 1
                    snake!!.timeFast_stack = 0
                    snake!!.timeFast+=30
                }
                snake!!.auto_move_stack+=1
                snake!!.timeFast_stack+=1
                view!!.postInvalidate()
            }
        }
        timer2 = Timer()
        snake = Snake_Game_Body(timer2!!)
        view!!.postInvalidate()
        timer2!!.schedule(task2, 0, 10)
        //MotionEvent.ACTION_MOVE, MotionEvent.ACTION_UP
        (view as ImageView).setOnTouchListener(OnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN-> {
                    // 이미지 뷰의 위치를 옮기기
                    x_start = event.x
                    y_start = event.y
                }
                MotionEvent.ACTION_UP->{
                    x_end = event.x
                    y_end = event.y
                    val x_differ = x_end - x_start
                    val y_differ = y_end - y_start
                    var x_size = 0.0F
                    var y_size = 0.0F
                    var Position = 0
                    if(x_differ < 0){
                        x_size = -x_differ
                    }else{
                        x_size = x_differ
                    }
                    if(y_differ < 0){
                        y_size = -y_differ
                    }else{
                        y_size = y_differ
                    }
                    if(x_size > y_size){
                        if(x_differ < 0){
                            Position = 2
                        }else{
                            Position = 3
                        }
                    }else{
                        if(y_differ < 0){
                            Position = 0
                        }else{
                            Position = 1
                        }
                    }
                    snake!!.ChangeMove(Position)
                }
            }
            true
        })
    }

    // 게임 보드의 크기는 20*20 으로 고정하자
    // 그렇다면 전체 판의 크기는 22 * 22 가 되어야한다 (상하좌우에 벽이 한 줄씩 있다.)
    //
    class Snake_Game_Body(var time : Timer){
        /*
            벽은 0으로 몸체는 1로 음식은 2로 저장해 놓는다.
         */
        var timeFast = 150
        var timeFast_stack = 0
        var auto_move = 50
        var auto_move_stack = 0
        val EMPTY = 0
        val WALL = 1
        val FOOD = 2
        val GAME_BOARD_SIZE = 22
        val SQUARE_SIZE = 40
        var head_Direction = 3 // 0은 위쪽, 1은 아래쪽, 2는 왼쪽 3은 오른쪽이라고 하자.
        var snake_length = 4
        var remainTail = 0
        val UP = 0
        var DOWN = 1
        var LEFT = 2
        var RIGHT = 3
        var timer : Timer = time
        var game_Over = 0
        /*
            뱀의 몸통위에서 아이템이 나오지 않는다는 확실한 보장이 있다면, 아이템을 먹을 수 있는것은 오직 뱀의 머리 뿐이다.
            즉 나머지 뱀의 몸통의 리스트를 가지고 있고, 머리의 방향을 가지고 있다면 굳이 다른 구조체가 필요 없다.
            나는 1차원 배열을 484개짜리 1차원 배열을 선언할 것이다 이것을 22로 나누어서 2차원 배열처럼 사용한다.
         */
        var gameBoard : Array<Int> = Array<Int>(GAME_BOARD_SIZE * GAME_BOARD_SIZE, {i->0} )
        var snakeBody : Array<Int> = Array<Int>(GAME_BOARD_SIZE*GAME_BOARD_SIZE,{i->-1}) // 뱀들의 위치를 나타낸다.
        init{
            for(i in 0 until GAME_BOARD_SIZE){
                gameBoard[i] = 1
                gameBoard[GAME_BOARD_SIZE*i] = 1
                gameBoard[GAME_BOARD_SIZE*i+(GAME_BOARD_SIZE-1)] = 1
                gameBoard[GAME_BOARD_SIZE*GAME_BOARD_SIZE-(GAME_BOARD_SIZE+1)+i] = 1
            }
            MakeFood()
            snakeBody[0] = 26
            snakeBody[1] = 25
            snakeBody[2] = 24
            snakeBody[3] = 23
        }

        fun MakeFood(){
            var r = -1
            do{
                var restart = false
                var random = Random()
                r = random.nextInt(GAME_BOARD_SIZE * ( GAME_BOARD_SIZE - 1))
                if(gameBoard[r] == WALL){
                    restart = true
                }
            }while(restart)
            gameBoard[r] = FOOD
        }

        fun Move() {
            // 뱀이 너무 길어져도 그냥 게임을 종료시킨다. 여기까지 오는건 사실 불가능하겠지만 말이야...
            if(snake_length > 430){
                timer.cancel()
            }
            // 뱀의 위치가 자신 앞에 있는 자의 위치를 참조해서 앞으로 한칸씩 이동한다
            remainTail = snakeBody[snake_length-1]
            for(i in snake_length-1 downTo 1){
                snakeBody[i] = snakeBody[i-1]
            }
            if(head_Direction == 0){
                snakeBody[0] -=22
            }else if(head_Direction == 1){
                snakeBody[0] +=22
            }else if(head_Direction == 2){
                snakeBody[0] -=1
            }else{
                snakeBody[0] +=1
            }
            if(CheckConflict()){
                timer.cancel()
                game_Over = 1
            }
            auto_move_stack = 0
        }
        fun ChangeMove(direction : Int){
            // 방향을 바꾸거나, 그 방향을 클릭했더라도 한칸은 반드시 간다.
            head_Direction = direction
            Move()
        }
        fun CheckConflict() : Boolean{
            //머리가 벽과 충돌했는지 검사
            //머리가 아이템과 충돌했는지 검사
            if(gameBoard[snakeBody[0]] == WALL){
                return true
            }else if(gameBoard[snakeBody[0]] == FOOD){
                snakeBody[snake_length] = remainTail
                snake_length+=1
                gameBoard[snakeBody[0]] = EMPTY
                MakeFood()
            }
            //머리가 다른 몸통과 충돌했는지 검사
            for(i in 1 until snake_length){
                if(snakeBody[0] == snakeBody[i]){
                    return true
                }
            }
            return false
        }
    }

    // 그리기를 처리하는 부분이다. 그리기 함수에서는 얘를 호출 할 수 있게 준비해야한다.
    inner class ImageView(context: Context) : View(context) {
        override fun onDraw(canvas: Canvas) {
            if(snake!!.game_Over == 1){
                snake!!.game_Over+=1
                var final_length =  snake!!.snake_length
                Toast.makeText(this@Snake_Game_Play, "게임 오버! 달성한 뱀의 길이 : $final_length" , Toast.LENGTH_SHORT).show()
                GameScoreDatabaseUsgae("snake", final_length.toDouble() , 0 ,context).SaveData()
            }
            val paint = Paint()
            paint.color = Color.GREEN
            for(i in 0 until snake!!.GAME_BOARD_SIZE){
                for(j in 0 until snake!!.GAME_BOARD_SIZE){
                    var color_check = snake!!.gameBoard[22*j+i]
                    if(color_check == 0){
                        paint.color = Color.WHITE
                    }else if(color_check == 1){
                        paint.color = Color.BLUE
                    }else if(color_check == 2){
                        paint.color = Color.GREEN
                    }
                    val moveRight = 100
                    val moveDown = 400
                    canvas.drawRect((i* snake!!.SQUARE_SIZE + moveRight).toFloat(),(j* snake!!.SQUARE_SIZE+moveDown).toFloat(),((i+1)* snake!!.SQUARE_SIZE + moveRight).toFloat(),((j+1)* snake!!.SQUARE_SIZE+moveDown).toFloat(), paint)
                    for(t in 0 until snake!!.snake_length){
                        paint.color = Color.RED
                        var i = snake!!.snakeBody[t] % snake!!.GAME_BOARD_SIZE
                        var j = (snake!!.snakeBody[t] / snake!!.GAME_BOARD_SIZE).toInt()
                        canvas.drawRect((i* snake!!.SQUARE_SIZE + moveRight).toFloat(),(j* snake!!.SQUARE_SIZE+moveDown).toFloat(),((i+1)* snake!!.SQUARE_SIZE + moveRight).toFloat(),((j+1)* snake!!.SQUARE_SIZE+moveDown).toFloat(), paint)
                    }
                }
            }

            super.onDraw(canvas)
        }
    }
}
