package com.example.q.myapplication

import android.content.Context
import android.view.View
import android.view.ViewGroup

class ImageGridAdapter {

    var context : Context? = null
    var imageIDs : Array<Int>? = null

    fun ImageGridAdapter(context : Context, imageIDs : Array<Int> ){
        this.context = context
        this.imageIDs = imageIDs
    }

    fun getCount(): Int {
        return imageIDs!!.size
    }

    fun getItem(position : Int): Int {
        return imageIDs!![position]
    }

    fun getItemID(position : Int): Int{
        return position
    }

    fun getView(position : Int, contentView : View, parent : ViewGroup){

    }


}