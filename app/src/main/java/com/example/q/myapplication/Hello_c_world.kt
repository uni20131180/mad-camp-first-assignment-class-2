package com.example.q.myapplication

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Point
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Button
import android.app.WallpaperManager
import android.widget.ImageView


class Hello_c_world : AppCompatActivity() {

    var REQUIRED_CONNECT = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello_c_world)


        var button1= findViewById(R.id.button1) as Button
        var button2= findViewById(R.id.button2) as Button
        var button3= findViewById(R.id.button3) as Button
        val view = findViewById(R.id.background) as ImageView
        view.setImageDrawable(WallpaperManager.getInstance(this).drawable)

        button1.setOnClickListener(View.OnClickListener {
            var permissionaddress = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_CONTACTS)
            if (permissionaddress == PackageManager.PERMISSION_DENIED) {
                REQUIRED_CONNECT = 1
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), 1)
            }else{
                val addressActivity = Intent(this, address_activity::class.java)
                startActivity(addressActivity)
            }
        })

        button2.setOnClickListener(View.OnClickListener {
            val permissionEX_Storage = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            if (permissionEX_Storage == PackageManager.PERMISSION_DENIED) {
                REQUIRED_CONNECT = 2
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
            }
            else {
                val galleryActivity = Intent(this, gallery_activity::class.java)
                var a= window.windowManager.defaultDisplay
                var b = Point()
                a.getSize(b)
                galleryActivity.putExtra("width", b.x)
                galleryActivity.putExtra("height", b.y)
                galleryActivity.putExtra("page", 1)
                startActivity(galleryActivity)
            }
        })
        button3.setOnClickListener(View.OnClickListener {
            val addressActivity = Intent(this, Menu_activity::class.java)
            startActivity(addressActivity)
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        var PERMISSION_REQUEST_CODE = 100
        var check_result = true
        // 모든 퍼미션을 허용했는지 체크합니다.
        if(requestCode == 1){
            for (result in grantResults) {
                println("result : $result")
                if (result != PackageManager.PERMISSION_GRANTED) {
                    check_result = false
                    break
                }
            }
            if (check_result) {
                // 모든 퍼미션을 허용했다면 REQUIRED_CONNECTED 에 따라서 해결한다.
                if(REQUIRED_CONNECT== 1){
                    val addressActivity = Intent(this, address_activity::class.java)
                    startActivity(addressActivity)
                }else if(REQUIRED_CONNECT==2){
                    val galleryActivity = Intent(this, gallery_activity::class.java)
                    var a= window.windowManager.defaultDisplay
                    var b = Point()
                    a.getSize(b)
                    galleryActivity.putExtra("width", b.x)
                    galleryActivity.putExtra("height", b.y)
                    galleryActivity.putExtra("page", 1)
                    startActivity(galleryActivity)
                }
            }
        }
    }
}
