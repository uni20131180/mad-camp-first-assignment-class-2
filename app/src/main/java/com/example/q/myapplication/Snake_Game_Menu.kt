package com.example.q.myapplication

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button

class Snake_Game_Menu : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_menu)
        setTitle("스네이크 (세베루스 아님)")

        val button1 = findViewById<Button>(R.id.gameplay)
        val button2 = findViewById<Button>(R.id.gamescore)
        val button3 = findViewById<Button>(R.id.gamehelp)

        button1.setOnClickListener(View.OnClickListener {
            val addressActivity = Intent(this, Snake_Game_Play::class.java)
            startActivity(addressActivity)
        })

        button2.setOnClickListener(View.OnClickListener {
            // 스코어 액티비티로 넘긴다.
            // 게임 액티비티 내에서 최고 스코어만 저장한 뒤, 불러온다.
            val addressActivity = Intent(this, Snake_Game_Scoreboard::class.java)
            startActivity(addressActivity)
        })

        button3.setOnClickListener(View.OnClickListener {
            // 게임 플레이 방법을 도움말에 대사로 첨부한다.
            // 여기다가 Fragment 를 사용한다.
        })

    }
}