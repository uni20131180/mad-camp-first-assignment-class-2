package com.example.q.myapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView

class Snake_Game_Scoreboard : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.snake_game_scoreboard)
        setTitle("스네이크 (세베루스 아님) 스코어보드")

        var dbLoader1 = GameScoreDatabaseUsgae("snake", 0.0, 0, this)
        var cursor1 = dbLoader1.GetAllData()

        var resultString = ""

        resultString += "Length \n"
        if (cursor1!!.moveToFirst()) {
            do {
                resultString += cursor1.getString(1) + " : " + cursor1.getDouble(2) + "\n"
            } while (cursor1!!.moveToNext())
        }

        var textV = findViewById<TextView>(R.id.snakescoreboard)
        textV.setText(resultString)
    }
}