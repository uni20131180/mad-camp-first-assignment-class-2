package com.example.q.myapplication

import android.graphics.Bitmap
import android.graphics.Canvas
import android.widget.Toast

class BGcharacter(image_bitmap:Bitmap, identifier_num : Int, start_x: Float, start_y: Float){
    var myimage_bitmap=image_bitmap
    var my_x = start_x
    var my_y = start_y
    var my_bullets = mutableListOf<BGcharacter>()
    var cw = image_bitmap.getWidth()          // 캐릭터의 폭/2
    var ch = image_bitmap.getHeight()        // 캐릭터의 높이/2
    var HP = 0

    init {
        if(identifier_num == 0){//메인 케릭터를 의미
            cw = cw/10
            ch = ch/10
            myimage_bitmap = Bitmap.createScaledBitmap(image_bitmap, cw,ch,false)
            HP = 5
        }

        else if(identifier_num==1){// 아군 총알 의미
            myimage_bitmap = Bitmap.createScaledBitmap(image_bitmap, 30,50,false)
            HP=1
        }

        else if(identifier_num == 2){//적 케릭터를 의미
            cw = cw/10
            ch = ch/10
            myimage_bitmap = Bitmap.createScaledBitmap(image_bitmap, cw, ch,false)
            HP=3
        }

        else if(identifier_num==3){// 아군 총알 의미
            myimage_bitmap = Bitmap.createScaledBitmap(image_bitmap, 50,50,false)
            HP=1
        }

        else if(identifier_num==4){// 보스 캐릭터를 의미
            cw=cw/10
            ch=ch/10
            myimage_bitmap = Bitmap.createScaledBitmap(image_bitmap, cw, ch,false)
            HP=10
        }
    }

    fun draw(canvas: Canvas){
        canvas.drawBitmap(myimage_bitmap, my_x , my_y, null)
    }

    fun shoot_above() {
        var deleted_num = 0
        for (z in 0 until my_bullets.size) {
            val current_missle = my_bullets[z-deleted_num]
            current_missle.my_y = current_missle.my_y - 12f
            if (current_missle.my_y < 0) {
                my_bullets.removeAt(z-deleted_num)
                deleted_num++
            }
        }
    }

    fun shoot_below(canvas: Canvas){
        var deleted_num = 0
        for (z in 0 until my_bullets.size) {
            val current_missle = my_bullets[z-deleted_num]
            current_missle.my_y = current_missle.my_y + 12f
            if (current_missle.my_y > canvas.height) {
                my_bullets.removeAt(z-deleted_num)
                deleted_num++
            }
        }
    }

    fun shoot(bitmap: Bitmap){
        my_bullets.add( BGcharacter(bitmap, 1, my_x, my_y))
    }

    fun shoot_enemy(bitmap: Bitmap){
        my_bullets.add( BGcharacter(bitmap, 3, my_x, my_y))
    }

    fun draw_bullet(canvas: Canvas){
        for(i in 0 until my_bullets.size){
            my_bullets[i].draw(canvas)
        }
    }

    fun bullet_check(enemy_bullet:MutableList<BGcharacter>){
        var number_bombed_bullet = 0
        for(i in 0 until enemy_bullet.size){
            val k = i-number_bombed_bullet
            val current_bullet = enemy_bullet[k]
            if(current_bullet.my_x  >= my_x && current_bullet.my_x <= my_x + cw){
                if(current_bullet.my_y >= my_y && current_bullet.my_y <= my_y + ch ){
                    HP--
                    number_bombed_bullet++
                    enemy_bullet.removeAt(k)
                }
            }
        }
    }
}