package com.example.q.myapplication

import android.content.Intent
import android.graphics.Point
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button

class slide_puzzle_game_difficult_choice : AppCompatActivity(), View.OnClickListener  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.slide_puzzle_choice_difficult)

        var idArray = arrayOf(R.id.slidegameeasy,R.id.slidegamenormal,R.id.slidegamehard,R.id.slidegameback)
        var size = idArray.size
        for(i in 0 until size){
            (findViewById(idArray[i]) as Button).setOnClickListener(this)
        }
    }

    override fun onClick(v: View?) {
        var boardSize = 3
        when(v!!.id){
            R.id.slidegameeasy,R.id.slidegamenormal,R.id.slidegamehard->{
                when(v!!.id){
                    R.id.slidegameeasy->{
                        boardSize = 3
                    }
                    R.id.slidegamenormal->{
                        boardSize = 4
                    }
                    R.id.slidegamehard->{
                        boardSize = 5
                    }
                }
                val g = Intent(this, slide_puzzle_game_play::class.java)
                g.putExtra("boardSize", boardSize)
                var a= window.windowManager.defaultDisplay
                var b = Point()
                a.getSize(b)
                g.putExtra("width", b.x)
                g.putExtra("height", b.y)
                startActivity(g)
            }
            R.id.slidegameback->{
                finish()
            }

        }
    }
}