package com.example.q.myapplication

import android.content.Context
import android.graphics.*
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.*
import android.support.v7.app.AppCompatActivity
import android.view.Window
import java.util.*
import android.view.WindowManager
import android.view.View
import android.widget.Toast


class dodge_game_play : AppCompatActivity(), SensorEventListener {
    private var sensorManager : SensorManager? = null
    private var accelerormeterSensor : Sensor? = null
    private var width  = 0
    private var height = 0
    var timer : Timer? = null
    var timer2 : Timer? = null
    var Game : DodgeGameClass? = null
    var view : View? = null
    var gameover = 0
    var startTime : Long? = null
    var endTime : Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow()
            .setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        view = ImageView(this)
        setContentView(view)
        setTitle("닷지")
        width = intent.getIntExtra("width", 320)
        height = intent.getIntExtra("height", 500)
        sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager?;
        accelerormeterSensor = sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Game = DodgeGameClass(width.toFloat(), height.toFloat())

        val task = object : TimerTask() {
            override fun run() {
                if (gameover == 0) {
                    Game!!.moveAllCharacter()
                    Game!!.drawGameBoard(view as ImageView)
                    if (Game!!.checkGameOver()) {
                        timer!!.cancel()
                        timer2!!.cancel()
                        gameover = 1
                    }
                }
            }
        }
        for (i in 1..30) {
            Game!!.makeBall()
        }
        val task2 = object : TimerTask() {
            override fun run() {
                Game!!.makeBall()
            }
        }
        timer = Timer()
        timer!!.schedule(task, 0, 16);
        timer2 = Timer()
        timer2!!.schedule(task2, 100, 1000)
        startTime = System.currentTimeMillis()
    }

    override fun onDestroy() {
        super.onDestroy()
        timer!!.cancel()
        timer2!!.cancel()
    }


    override fun onStart() {
        super.onStart()
        if (accelerormeterSensor != null){
            sensorManager!!.registerListener(this, accelerormeterSensor, SensorManager.SENSOR_DELAY_GAME)
        }
    }

    override fun onStop(){
        super.onStop()
        if(sensorManager != null) {
            sensorManager!!.unregisterListener(this)
        }
    }


    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }

    override fun onSensorChanged(event: SensorEvent?) {
        val accX = event!!.values[0]
        val accY = event!!.values[1]

        Game!!.movePlayer(accX,accY)
        /*
        Log.e(
            "LOG", "ACCELOMETER           [X]:" + String.format("%.4f", event.values[0])
                    + "           [Y]:" + String.format("%.4f", event.values[1])
                    + "           [Z]:" + String.format("%.4f", event.values[2])
                    + "           [angleXZ]: " + String.format("%.4f", angleXZ)
                    + "           [angleYZ]: " + String.format("%.4f", angleYZ)
        )

        좌 우로 회전이 x방향, 전 후로 회전이 y방향이다.
        가속도 센서의 입력을 잘 받아서
        */
    }

    class DodgeGameClass(x_position: Float, y_position: Float){
        var character : Character? = null
        var balls : Array<Ball?>? = null
        var ball_count = 0 // 현재 필드에서 떠돌아다니고 있는 공의 갯수를 기록한다.
        var width = x_position // 게임판의 가로길이
        var height = y_position // 게임판의 세로길이
        init{
            character = Character(x_position = x_position, y_position = y_position)
            balls = arrayOfNulls<Ball>(100)
        }
        class Ball(x_position: Float,y_position: Float,x_speed:Float, y_speed:Float){
            var ball_size = 15F
            var x_position = x_position // 공은 가속도가 없이 일정한 속도로 움직이게 한다고 하자. 그럼 가속도를 처리하지 않아도 되서 게임 구현이 간단해진다.
            var y_position = y_position
            var x_speed = x_speed
            var y_speed = y_speed
        } // 캐릭터가 아닌 공의 객체
        class Character(x_position : Float, y_position:Float){
            var character_size = 15F
            var x_position = x_position/2
            var y_position = y_position/2
            var x_speed = 1.0
            var y_speed = 1.0
            var x_acclerate = 0.0
            var y_acclerate = 0.0
        } // 캐릭터의 객체, 캐릭터는 오직 하나만 존재한다.
        fun makeBall(){
            // 공을 새로 만들고 임의의 속도를 지니게 해서 화면으로 방출한다. 이때 경계에서 나오는것은 보장되어있다.
            if(ball_count >= 100){

            }else{
                var random = Random()
                var x_speed = random.nextFloat() * 4
                var y_speed = random.nextFloat() * 4
                var negative1 = random.nextInt(2)
                var negative2 = random.nextInt(2)
                if(negative1==1){
                    x_speed = -x_speed
                }
                if(negative2==1){
                    y_speed = -y_speed
                }
                balls!![ball_count] = Ball(width,height,x_speed,y_speed)
                ball_count++
            }
        }
        fun moveAllCharacter(){
            // 캐릭터의 움직임을 결정
            character!!.x_speed += character!!.x_acclerate
            character!!.y_speed += character!!.y_acclerate
            character!!.x_position += character!!.x_speed.toFloat()
            character!!.y_position += character!!.y_speed.toFloat()
            if(character!!.x_position > width){
                character!!.x_position -= width
            }else if(character!!.x_position < 0){
                character!!.x_position += width
            }
            if(character!!.y_position > height - 40){
                character!!.y_position -= (height - 70)
            }else if(character!!.y_position < 30){
                character!!.y_position += (height - 70)
            }
            for(i in 0 until ball_count){
                balls!![i]!!.x_position += balls!![i]!!.x_speed
                balls!![i]!!.y_position += balls!![i]!!.y_speed
                if(balls!![i]!!.x_position > width){
                    balls!![i]!!.x_position -= width
                }else if(balls!![i]!!.x_position < 0){
                    balls!![i]!!.x_position += width
                }
                if(balls!![i]!!.y_position > height - 40){
                    balls!![i]!!.y_position -= (height - 70)
                }else if(balls!![i]!!.y_position < 30){
                    balls!![i]!!.y_position += (height - 70)
                }
            }

        } // 프레임당 캐릭터들의 움직임을 결정한다. 프레임마다 이 함수와 drawGameBoard 함수를 호출해야한다.
        fun drawGameBoard(view : View){
            view.postInvalidate()
        } // 게임보드를 갱신한다. 프레임마다 이 함수와 moveAllCharacter 함수를 호출해야한다.
        fun movePlayer(accX : Float ,accY : Float){
            character!!.x_acclerate = -accX/100.0
            character!!.y_acclerate = accY/100.0
        } // 플레이어의 이동을 플레이어 객체에 저장해놓는다.
        fun checkGameOver(): Boolean {

            var player_x_position = character!!.x_position
            var player_y_position = character!!.y_position
            for(i in 0 until ball_count){
                // 모든 공에 대해서 충돌 하는지 하지 않는지 검사한다.
                // 원이기 때문에 두 원의 지름을 더한 30보다 거리가 가까우면 그 즉시 true 를 반환한다.
                var distanceSquare = ((balls!![i]!!.x_position - player_x_position) * (balls!![i]!!.x_position - player_x_position)) + ((balls!![i]!!.y_position - player_y_position) * (balls!![i]!!.y_position - player_y_position))
                if(distanceSquare < 835){
                    return true
                }
            }

            return false
        } // 게임오버를 체크한다, 이 게임에서는 플레이어와 논 플레이어 객체가 부딫혔는지 검사한다.

    }

    inner class ImageView(context: Context) : View(context) {
        override fun onDraw(canvas: Canvas) {
            val paint = Paint()
            paint.color = Color.BLUE
            canvas.drawCircle(Game!!.character!!.x_position, Game!!.character!!.y_position, 15F, paint)
            paint.color = Color.RED
            for(i in 0 until Game!!.ball_count){
                canvas.drawCircle(Game!!.balls!![i]!!.x_position.toFloat(), Game!!.balls!![i]!!.y_position.toFloat(), 15F, paint)
            }
            if(gameover == 1){
                endTime = System.currentTimeMillis()
                var finalTime = ((endTime!!-startTime!!)/1000.0)
                Toast.makeText(this@dodge_game_play, "$finalTime 초 동안 버텼습니다." , Toast.LENGTH_SHORT).show()
                GameScoreDatabaseUsgae("dodge", finalTime , 0 ,context).SaveData()
                gameover+=1
            }
            super.onDraw(canvas)
        }
    }
}