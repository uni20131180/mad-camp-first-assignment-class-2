package com.example.q.myapplication

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button

class address_matching_game_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_menu)

        val button1 = findViewById<Button>(R.id.gameplay)
        val button2 = findViewById<Button>(R.id.gamescore)
        val button3 = findViewById<Button>(R.id.gamehelp)

        setTitle("전화번호/이름 맞추기")

        button1.setOnClickListener(View.OnClickListener {
            // 게임 액티비티로 옮겨준다.
            // 게임 액티비티 내에서 만일 전화번호부에 전화번호가 10개 이하라면 게임 실행을 reject 한다.
            // 지금 구상으로는 10문제가 나오는데
            // 번호를 보고 이름 맞추기 (객관식 5문항)
            // 이름을 보고 번호 맞추기 (객관식 5문항)
            // 정답과 오답 모두 전화번호부에서 랜덤하게 들고온다.
            // 게임 플레이 후 스코어를 합산해서 저장한다.
            val addressActivity = Intent(this, address_matching_game_play::class.java)
            startActivity(addressActivity)

        })

        button2.setOnClickListener(View.OnClickListener {
            // 스코어 액티비티로 넘긴다.
            // 게임 액티비티 내에서 최고 스코어만 저장한 뒤, 불러온다.
            val addressActivity = Intent(this, address_matching_game_scoreboard::class.java)
            startActivity(addressActivity)
        })

        button3.setOnClickListener(View.OnClickListener {
            // 게임 플레이 방법을 도움말에 대사로 첨부한다.
            // 여기다가 Fragment 를 사용한다.
        })

    }
}