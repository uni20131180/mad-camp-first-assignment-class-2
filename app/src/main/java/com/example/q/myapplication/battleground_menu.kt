package com.example.q.myapplication

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button

class battleground_menu: AppCompatActivity(), View.OnClickListener   {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_menu)
        setTitle("BattleGround")
        /*
            이벤트 리스너를 한번에 깔끔하게 달 수 있다. 읽기 쉽고 유지보수 하기 쉬운 코드이다.
         */
        var idArray = arrayOf(R.id.gameplay, R.id.gamescore,R.id.gamehelp)
        var size = idArray.size
        for(i in 0 until size){
            (findViewById(idArray[i]) as Button).setOnClickListener(this)
        }
    }
    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.gameplay->{
                startActivity(Intent(this, battleground_activity::class.java))
            }
            R.id.gamescore->{
                startActivity(Intent(this, battleground_scoreboard::class.java))
            }
            R.id.gamehelp->{
                println("Game Help")
            }
        }
    }
}