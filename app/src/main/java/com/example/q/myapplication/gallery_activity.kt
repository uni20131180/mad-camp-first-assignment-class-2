package com.example.q.myapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.provider.MediaStore
import kotlinx.android.synthetic.main.gallery.*
import android.content.Intent
import android.graphics.Bitmap
import android.view.View
import android.widget.AdapterView
import android.view.MotionEvent

class gallery_activity : AppCompatActivity()  {

    var width = 50
    var height = 100
    var page = 1
    var distance = 0.0F
    var x_Start = 0.0F
    var y_Start = 0.0F
    var x_Change = 0.0F
    var y_Change = 0.0F
    var changeActivity = 0
    var endpage = 0
    var onepage = 30
    var lastPage = 0;
    var renewal = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        width = intent.getIntExtra("width",1000)
        height = intent.getIntExtra("height",2000)
        page = intent.getIntExtra("page",1)
        println("width : $width  height :  $height")
        setContentView(R.layout.gallery)
        drawFunction()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        renewal = resultCode
        println(renewal)
    }

    override fun onResume() {
        super.onResume()
        if(renewal==1){
            drawFunction()
        }
        renewal = 0
    }
    fun drawFunction(){

        var PhotoList = arrayListOf<PhoneGallery>()
        val projection = arrayOf(
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.DATA,
            MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.SIZE
        )
        var firstPage = 0 + onepage*(page-1)
        var secondPage = onepage
        val sortOrder = "_display_name DESC LIMIT "+ firstPage.toString() + ", " + secondPage.toString()
        val totalSize = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, null)
        endpage = totalSize.count
        if( firstPage + onepage >= endpage ){
            setTitle("My Gallery " + (firstPage + 1) + " - " + endpage + " / " + endpage)
            lastPage = 1
        }else{
            setTitle("My Gallery " + (firstPage + 1) + " - " + ( (firstPage) + onepage ) + " / " + endpage)
        }

        val imageCursor = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, sortOrder)
        if (imageCursor.moveToFirst()) {
            do {
                var mediaID =  imageCursor.getString(0)
                var mediaData = imageCursor.getString(1)
                var mediaDisplay = imageCursor.getString(2)
                var mediaSize = imageCursor.getString(3)
                //var bitmap : Bitmap = BitmapFactory.decodeFile(mediaData,options)
                //var rebitmap = Bitmap.createScaledBitmap(bitmap, width/3,width/3,false)
                var bitmap = MediaStore.Images.Thumbnails.getThumbnail(contentResolver,mediaID.toLong(),MediaStore.Images.Thumbnails.MINI_KIND,null)
                bitmap = Bitmap.createScaledBitmap(bitmap, width/3,width/3,false)
                /*
                if (bitmap.getWidth() >= bitmap.getHeight()){
                    bitmap = Bitmap.createBitmap(bitmap, bitmap.getWidth()/2 - bitmap.getHeight()/2, 0, bitmap.getHeight(), bitmap.getHeight())
                }else{
                    bitmap = Bitmap.createBitmap(bitmap, 0, bitmap.getHeight()/2 - bitmap.getWidth()/2, bitmap.getWidth(), bitmap.getWidth())
                }
                */
                var a = PhoneGallery(mediaID = mediaID, mediaData= mediaData, mediaDisplay = mediaDisplay, mediaSize = mediaSize, bitmap = bitmap)
                PhotoList.add(a)
                /*
                     println("ID : " + mediaID + "  Data : " + mediaData + " mediaDisplay : " + mediaDisplay + " media size : " + mediaSize)
                */
            } while (imageCursor.moveToNext())
        }
        val mAdapter = galleryAdapter(this, PhotoList, width, height)
        gallerygrid.adapter = mAdapter
        val onePictureActivity = Intent(this, One_picture::class.java)
        gallerygrid.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if(changeActivity == 0){
                    onePictureActivity.putExtra("fileID",PhotoList[id.toInt()].mediaID)
                    onePictureActivity.putExtra("fileURL", PhotoList[id.toInt()].mediaData)
                    onePictureActivity.putExtra("fileName", PhotoList[id.toInt()].mediaDisplay)
                    startActivityForResult(onePictureActivity,0)
                }
            }
        }
        gallerygrid.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN ->{
                    x_Start = event.getX()
                    y_Start = event.getY()
                }
                MotionEvent.ACTION_MOVE->{

                }
                MotionEvent.ACTION_UP -> {
                    x_Change = event.getX() - x_Start
                    y_Change = event.getY() - y_Start
                    if(y_Change > -200 && y_Change < 200){
                        if(x_Change > 85 || x_Change < -85){
                            if(x_Change > 85){
                                changeActivity = 1
                                if(page > 1){
                                    val galleryActivity = Intent(this, gallery_activity::class.java)
                                    galleryActivity.putExtra("width", width)
                                    galleryActivity.putExtra("height", height)
                                    galleryActivity.putExtra("page", page-1)
                                    finish()
                                    startActivity(galleryActivity)
                                    overridePendingTransition(android.R.anim.slide_in_left, R.anim.hold)
                                }
                            }else {
                                if(lastPage == 0){
                                    val galleryActivity = Intent(this, gallery_activity::class.java)
                                    galleryActivity.putExtra("width", width)
                                    galleryActivity.putExtra("height", height)
                                    galleryActivity.putExtra("page", page + 1)
                                    finish()
                                    startActivity(galleryActivity)
                                }
                            }
                            return@setOnTouchListener true
                        }
                    }
                }
            }
            super.onTouchEvent(event)
        }
    }
}
