package com.example.q.myapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView

class slide_puzzle_game_scoreboard : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.slide_puzzle_scoreboard)
        var dbLoader1 = GameScoreDatabaseUsgae("slidegameeasy", 0.0, 1, this)
        var dbLoader2 = GameScoreDatabaseUsgae("slidegamenormal", 0.0, 1, this)
        var dbLoader3 = GameScoreDatabaseUsgae("slidegamehard", 0.0, 1, this)
        var cursor1 = dbLoader1.GetAllData()
        var cursor2 = dbLoader2.GetAllData()
        var cursor3 = dbLoader3.GetAllData()

        var resultString = ""

        resultString += "EASY \n"
        if (cursor1!!.moveToFirst()) {
            do {
                resultString += cursor1.getString(1) + " : " + cursor1.getDouble(2) + "\n"
            } while (cursor1!!.moveToNext())
        }
        resultString += "NORMAL \n"
        if (cursor2!!.moveToFirst()) {
            do {
                resultString += cursor2.getString(1) + " : " + cursor2.getDouble(2) + "\n"
            } while (cursor2!!.moveToNext())
        }
        resultString += "HARD \n"
        if (cursor3!!.moveToFirst()) {
            do {
                resultString += cursor3.getString(1) + " : " + cursor3.getDouble(2) + "\n"
            } while (cursor3!!.moveToNext())
        }

        var textV = findViewById<TextView>(R.id.slidepuzzlescore)
        textV.setText(resultString)


    }
}