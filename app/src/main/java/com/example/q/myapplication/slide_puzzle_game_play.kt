package com.example.q.myapplication

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.GridView
import android.widget.Toast
import java.util.*

class slide_puzzle_game_play: AppCompatActivity() {

    var gameBoard : GridView? = null
    var pieceOfPicture : Array<picturePiece?>? = null
    var clear = false
    class picturePiece(bitmap: Bitmap, order:Int, empty:Boolean) {
        var picture= bitmap
        var order = order
        var empty = empty
    }
    var startTime = System.currentTimeMillis()
    var endTime = System.currentTimeMillis()

    var c = this
    var boardSize = 3
    var width = 100
    var height = 100
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.slide_puzzle_game_board)
        gameBoard = findViewById<GridView>(R.id.slidegameboard)

        // 사진을 랜덤하게 하나 가지고 오는 코드
        boardSize = intent.getIntExtra("boardSize",3)
        width = intent.getIntExtra("width",100)
        val projection = arrayOf(
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.DATA,
            MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.SIZE
        )
        val totalSize = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, null)
        var size = totalSize.count
        var random = Random()
        var chooseRandom = random.nextInt((size-1))

        totalSize.moveToFirst()
        totalSize.move(chooseRandom)
        var addressOfPicture = totalSize.getString(1)
        var picture = BitmapFactory.decodeFile(addressOfPicture)
        picture = Bitmap.createScaledBitmap(picture,width,width,false)
        pieceOfPicture = arrayOfNulls<picturePiece>(boardSize*boardSize)
        // 여기까지 사진을 가지고 오는 작업이다.

        var pictureWidth = width/boardSize
        var pictureHeight = width/boardSize
        // 사진을 n*n 개로 나누는 작업
        for(i in 0 until boardSize){
            for(j in 0 until boardSize){
                if(i==boardSize-1 && j==boardSize-1){
                    var pic = picturePiece(Bitmap.createBitmap(pictureWidth, pictureHeight, Bitmap.Config.ARGB_8888) ,i*boardSize+j,true)
                    pieceOfPicture!![i*boardSize+j] = pic
                }else{
                    var pic = picturePiece(Bitmap.createBitmap(picture,j*pictureWidth,i*pictureWidth, pictureWidth, pictureHeight),i*boardSize+j,false)
                    pieceOfPicture!![i*boardSize+j] = pic
                }
            }
        }
        MakeGameBoard()
        DrawGameBoard()
        gameBoard!!.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                CheckAndMove(position)
                DrawGameBoard()
                // 클리어 했는지 체크한다.
                var check = true
                for(i in 0 until boardSize*boardSize-1){
                    if(pieceOfPicture!![i]!!.order != i){
                        check = false
                        break
                    }
                }
                if(check){
                    if(!clear){
                        endTime = System.currentTimeMillis()
                    }
                    var spendTime = (endTime - startTime)/1000.0
                    Toast.makeText(c, "축하합니다 모두 맞추셨습니다. 걸린 시간은 : $spendTime 초 입니다." , Toast.LENGTH_SHORT).show()
                    pieceOfPicture!![boardSize*boardSize-1] = picturePiece(Bitmap.createBitmap(picture,(boardSize-1)*pictureWidth,(boardSize-1)*pictureWidth, pictureWidth, pictureHeight),(boardSize*boardSize-1),false)
                    DrawGameBoard()
                    if(!clear){
                        saveRecord(spendTime)
                    }
                    clear = true
                }
            }
        }
    }

    fun saveRecord(spendTime:Double){
        //기록 저장을 위한 데이터베이스가 존재하는지 먼저 탐색한다.
        //데이터베이스가 존재하지 않는다면

        //초급 중급 고급에 따라서 데이터를 저장한다.
        if(boardSize == 3){
            GameScoreDatabaseUsgae("slidegameeasy",spendTime, 1 ,this).SaveData()
        }else if(boardSize == 4){
            GameScoreDatabaseUsgae("slidegamenormal",spendTime, 1 ,this).SaveData()
        }else if(boardSize == 5){
            GameScoreDatabaseUsgae("slidegamehard",spendTime, 1 ,this).SaveData()
        }
    }

    fun MakeGameBoard(){
        // 게임판을 초기화 하는 과정이다.
        // 만일 게임판을 만들었는데 맞추지 못하는 케이스가 나온다면 새로 다시 만들어야한다. 맞출 수 있는 게임판이 나올 때 까지 반복한다.
        var possible = false
        var random = Random()
        while(possible == false){
            for(i in 0 until boardSize * boardSize){
                var k = random.nextInt(boardSize*boardSize)
                var l = random.nextInt(boardSize*boardSize)
                var b = pieceOfPicture!![k]
                pieceOfPicture!![k] = pieceOfPicture!![l]
                pieceOfPicture!![l] = b
            }
            var reverseCount = 0
            for(i in 0 until boardSize * boardSize){
                for(j in i+1 until boardSize * boardSize){
                    if(pieceOfPicture!![i]!!.order > pieceOfPicture!![j]!!.order){
                        if((pieceOfPicture!![i]!!.order == boardSize * boardSize - 1) || (pieceOfPicture!![j]!!.order == boardSize * boardSize -1)){
                            continue
                        }
                        reverseCount+=1
                    }
                }
            }
            if(reverseCount%2==0){
                possible = true
            }
            startTime = System.currentTimeMillis()
        }
    }

    fun DrawGameBoard(){
        val mAdapter = slide_puzzle_game_play_adapter(c, pieceOfPicture!!)
        gameBoard!!.adapter = mAdapter
        gameBoard!!.numColumns = boardSize
    }

    fun CheckAndMove(position : Int){
        var up = position - boardSize
        var down = position + boardSize
        var left = -1
        var right = boardSize * boardSize
        if(position % boardSize != 0){
            left = position - 1
        }
        if(position % boardSize != boardSize-1){
            right = position + 1
        }
        // 왼쪽과 오른쪽은 -1 이라면 가장 왼쪽 가장 오른쪽 이라서... 교환 하면 안된다.
        // 위 아래는 범위 검사 해서범위 내에 있다면 빈칸이 있는지 검사 한 후 변경한다.
        if(up < 0){}else{
            if(pieceOfPicture!![up]!!.empty == true){
                var temp = pieceOfPicture!![up]
                pieceOfPicture!![up] = pieceOfPicture!![position]
                pieceOfPicture!![position] = temp
            }
        }
        if(down > boardSize * boardSize - 1){}else{
            if(pieceOfPicture!![down]!!.empty == true){
                var temp = pieceOfPicture!![down]
                pieceOfPicture!![down] = pieceOfPicture!![position]
                pieceOfPicture!![position] = temp
            }
        }
        if(left < 0){}else{
            if(pieceOfPicture!![left]!!.empty == true){
                var temp = pieceOfPicture!![left]
                pieceOfPicture!![left] = pieceOfPicture!![position]
                pieceOfPicture!![position] = temp
            }
        }
        if(right > boardSize * boardSize - 1){}else{
            if(pieceOfPicture!![right]!!.empty == true){
                var temp = pieceOfPicture!![right]
                pieceOfPicture!![right] = pieceOfPicture!![position]
                pieceOfPicture!![position] = temp
            }
        }
    }


}