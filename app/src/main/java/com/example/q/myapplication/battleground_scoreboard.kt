package com.example.q.myapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView

class battleground_scoreboard : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.battleground_scoreboard)
        var dbLoader = GameScoreDatabaseUsgae("BattleGround", 0.0, 0, this)
        var cursor = dbLoader.GetAllData()
        var resultString = ""
        resultString += "Ranking\n"
        if (cursor!!.moveToFirst()) {
            do {
                resultString += cursor.getString(1) + " : " + cursor.getDouble(2) + "\n"
            } while (cursor!!.moveToNext())
        }
        var textV = findViewById<TextView>(R.id.scoreboard)
        textV.setText(resultString)
    }

}