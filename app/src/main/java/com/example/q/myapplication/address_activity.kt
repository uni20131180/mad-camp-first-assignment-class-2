package com.example.q.myapplication

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.address.*


class address_activity : AppCompatActivity()  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.address)
        setTitle("연락처")
        DrawPicture()

        var adding_button= findViewById(R.id.add_button) as Button

        adding_button.setOnClickListener(View.OnClickListener {
            var permissionaddress_w = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.WRITE_CONTACTS)
            if (permissionaddress_w == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_CONTACTS), 1)
            }else {
                val add_addressActivity = Intent(this, add_address_activity::class.java)
                startActivity(add_addressActivity)
            }
        })
    }

    override fun onRestart() {
        super.onRestart()
        DrawPicture()
    }

    fun DrawPicture(){
        var phoneList = arrayListOf<PhoneAddress>()
        var uri : Uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        val projection = arrayOf(
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, // 연락처 이름
            ContactsContract.CommonDataKinds.Phone.NUMBER, // 연락처
            ContactsContract.CommonDataKinds.Phone._ID
        )
        val sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC"
        val contactCursor = contentResolver.query(uri, projection, null, null, sortOrder)
        println("Cursor : " + contactCursor)
        if (contactCursor.moveToFirst()) {
            do {

                var phonenumber = contactCursor.getString(0)
                var phonename = contactCursor.getString(1)
                var phoneid = contactCursor.getString(2)

                var a = PhoneAddress(PhoneNumber = phonenumber, name = phonename, id = phoneid)
                phoneList.add(a)

                /*
                    println("phonenumber : " + phonenumber + "  name : " + phonename)
                 */
            } while (contactCursor.moveToNext())

            val mAdapter = AddressAdapter(this, phoneList)
            view1.adapter = mAdapter

            val lm = LinearLayoutManager(this)
            view1.layoutManager = lm
            view1.setHasFixedSize(true)

        }
    }
}
