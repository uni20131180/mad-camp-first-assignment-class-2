package com.example.q.myapplication

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.support.v7.app.AlertDialog
import android.widget.EditText
import android.widget.Toast

class GameScoreDatabaseUsgae(TableName : String, Score : Double, Selection : Int, context : Context) {

    var TableName = TableName
    var Score = Score
    var sqliteDB :SQLiteDatabase? = null
    var selection = Selection
    var dataCursor : Cursor? = null
    var context = context
    init{
        OpenDBAndTable()
    }
    fun OpenDBAndTable(){

        sqliteDB = context.openOrCreateDatabase("GameScore.db", 0 ,null)
        var query = "CREATE TABLE IF NOT EXISTS $TableName (PK INTEGER PRIMARY KEY autoincrement,UserName TEXT, Score REAL)"
        sqliteDB!!.execSQL(query)
    }

    fun GetDataAccount(): Int {
        var query = "SELECT * FROM $TableName"
        dataCursor = sqliteDB!!.rawQuery(query, null)
        return dataCursor!!.count
    }

    fun GetAllData(): Cursor? {
        //0은 숫자가 낮은게 나쁜 기록이라고 간주하고 반환
        //1은 숫자가 높은게 나쁜 기록이라고 간주하고 반환
        //커서형을 반환하므로 받는쪽에서도 커서형으로 직접 순회해서 데이터를 Binding 할 필요가 있다.
        var query = ""
        query = if(selection == 0){
            "SELECT * FROM $TableName ORDER BY Score DESC"
        }else{
            "SELECT * FROM $TableName ORDER BY Score ASC"
        }
        dataCursor = sqliteDB!!.rawQuery(query,null)
        return dataCursor
    }

    fun FindLowScore(): Double {
        //0은 숫자가 낮은게 나쁜 기록이라고 간주하고 반환
        //1은 숫자가 높은게 나쁜 기록이라고 간주하고 반환
        var query = "SELECT * FROM $TableName ORDER BY Score ASC"
        dataCursor = sqliteDB!!.rawQuery(query,null)
        if(selection == 0){
            dataCursor!!.moveToFirst()
        }else{
            dataCursor!!.moveToLast()
        }
        return dataCursor!!.getDouble(2) // 저장되어있는 5개의 최고점수 중 가장 낮은 점수를 반환한다.
    }

    fun FindLowIndex(): Int {
        //0은 숫자가 낮은게 나쁜 기록이라고 간주하고 반환
        //1은 숫자가 높은게 나쁜 기록이라고 간주하고 반환
        var query = "SELECT * FROM $TableName ORDER BY Score ASC"
        dataCursor = sqliteDB!!.rawQuery(query,null)
        if(selection == 0){
            dataCursor!!.moveToFirst()
        }else{
            dataCursor!!.moveToLast()
        }
        return dataCursor!!.getInt(0) // 저장되어있는 5개의 최고점수 중 가장 낮은 점수가 있는 인덱스를 반환한다.
    }

    fun SaveData() {
        //0은 숫자가 낮은게 나쁜 기록이라고 간주하고 반환
        //1은 숫자가 높은게 나쁜 기록이라고 간주하고 반환
        //점수를 받은 뒤 이 점수가 스코어보드에 들어갈 자격이 있는 점수라면
        //이름을 받는 메시지창을 띄운다.
        //그 뒤 이름과 함께 데이터를 저장한다.
        var is_save = false
        if (GetDataAccount() < 5) {
            // 데이터의 갯수가 5보다 작다면 무조건 저장해야한다.
            is_save = true
        } else {
            // 데이터의 갯수가 5보다 크다면 최솟값과 비교해야한다.
            if (selection == 0) {
                // 낮은게 나쁜 기록일 경우, 최솟값보다 커야한다.
                if (FindLowScore() < Score) {
                    is_save = true
                }
            } else {
                if (FindLowScore() > Score) {
                    is_save = true
                }
            }
        }

        if (is_save) {
            // 저장해야 하는 데이터가 맞다면, 이름을 받는다.
            val ad = AlertDialog.Builder(context)
            ad.setTitle("축하합니다 기록을 달성하셨습니다.")       // 제목 설정
            ad.setMessage("이름을 입력해주세요!\n\n점수: $Score")   // 내용 설정
            // EditText 삽입하기
            val et = EditText(context)
            ad.setView(et)
            // 취소 버튼 설정
            ad.setNegativeButton("저장하지 않는다") { dialog, which ->
                println("저장하지 않아요!")
                dialog.dismiss()     //닫기
            }
            // 확인 버튼 설정
            ad.setPositiveButton("저장") { dialog, which ->
                val value = et.text.toString()
                if (value.length == 0) {
                    Toast.makeText(context, "이름을 채워주세요!", Toast.LENGTH_SHORT).show()
                } else {
                    // 저장버튼을 눌렀다면 String 을 받아와서 데이터베이스의 최솟값을 이 값으로 대체한다.
                    println(value)
                    if(GetDataAccount() < 5){
                        // 5보다 저장되어있는 값의 갯수가 적다면...
                        // 그냥 저장한다.
                        var query = "INSERT INTO $TableName (UserName, Score) VALUES ( '$value', $Score)"
                        sqliteDB!!.execSQL(query)
                        Toast.makeText(context, "기록 저장 완료", Toast.LENGTH_SHORT).show()
                    }else{
                        var lowIndex = FindLowIndex()
                        var query = "UPDATE $TableName SET UserName = '$value', Score = $Score where PK = $lowIndex"
                        sqliteDB!!.execSQL(query)
                        Toast.makeText(context, "기록 교체 완료", Toast.LENGTH_SHORT).show()
                    }
                    dialog.dismiss()     //닫기
                }
            }
            // 창 띄우기
            ad.show()
        }
    }
}