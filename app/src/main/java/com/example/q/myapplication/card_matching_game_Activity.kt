package com.example.q.myapplication

import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import java.util.*


class card_matching_game_Activity : AppCompatActivity() {

    var random_orders = shuffle(mutableListOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ,11, 12, 13, 14, 15), mutableListOf())
    var state_overturn = mutableListOf(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    var chance = 0
    var number_of_answer = 0
    var order_of_picked_before =0
    var location_of_picked_before=0
    var a = arrayOfNulls<ImageButton>(16)
    var width = 50
    var height = 100
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        width = intent.getIntExtra("width", 1000)
        height = intent.getIntExtra("height", 2000)

        setContentView(R.layout.card_matching_game)

        a[0] = findViewById(R.id.imageButton1) as ImageButton
        a[1] = findViewById(R.id.imageButton2) as ImageButton
        a[2] = findViewById(R.id.imageButton3) as ImageButton
        a[3] = findViewById(R.id.imageButton4) as ImageButton
        a[4] = findViewById(R.id.imageButton5) as ImageButton
        a[5] = findViewById(R.id.imageButton6) as ImageButton
        a[6] = findViewById(R.id.imageButton7) as ImageButton
        a[7] = findViewById(R.id.imageButton8) as ImageButton
        a[8] = findViewById(R.id.imageButton9) as ImageButton
        a[9] = findViewById(R.id.imageButton10) as ImageButton
        a[10] = findViewById(R.id.imageButton11) as ImageButton
        a[11] = findViewById(R.id.imageButton12) as ImageButton
        a[12] = findViewById(R.id.imageButton13) as ImageButton
        a[13] = findViewById(R.id.imageButton14) as ImageButton
        a[14] = findViewById(R.id.imageButton15) as ImageButton
        a[15] = findViewById(R.id.imageButton16) as ImageButton



        var PhotoList = arrayListOf<PhoneGallery>()
        val projection = arrayOf(
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.DATA,
            MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.SIZE
        )

        val imageCursor = contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            projection,
            null,
            null,
            null
        )
        var imageCursorSize = imageCursor.count
        var random_game = make_random(imageCursorSize, mutableListOf())
        var count = 0
        do {
            imageCursor.moveToFirst()
            imageCursor.move(random_game[count])
            count++
            var mediaID = imageCursor.getString(0)
            var mediaData = imageCursor.getString(1)
            var mediaDisplay = imageCursor.getString(2)
            var mediaSize = imageCursor.getString(3)
            var bitmap: Bitmap = MediaStore.Images.Thumbnails.getThumbnail(contentResolver,mediaID.toLong(),MediaStore.Images.Thumbnails.MINI_KIND,null)
            var rebitmap = Bitmap.createScaledBitmap(bitmap, width / 4, width / 4, false)
            var a = PhoneGallery(
                mediaID = mediaID,
                mediaData = mediaData,
                mediaDisplay = mediaDisplay,
                mediaSize = mediaSize,
                bitmap = rebitmap
            )
            PhotoList.add(a)
            /*
               println("ID : " + mediaID + "  Data : " + mediaData + " mediaDisplay : " + mediaDisplay + " media size : " + mediaSize)
            */
        } while (count<8)

        for (i in 0..8){
            PhotoList.add(PhotoList[i])
        }
        for(i in 0..15){
            reset(i)
        }
        for (i in 0 until a.size) {
            a[i]!!.setOnClickListener(View.OnClickListener {
                val order = random_orders[i]
                if (state_overturn[i] != 1) {
                    state_overturn[i]++
                    show_Picture(a[i], order, PhotoList)
                    chance++
                    if (chance % 2 == 1) {
                        order_of_picked_before = order
                        location_of_picked_before = i
                    } else {
                        if (order == order_of_picked_before + 8 || order == order_of_picked_before - 8) {
                            number_of_answer++
                            if (number_of_answer == 8) {
                                show_Clear_box()
                                saveRecord(chance/2)
                            }
                        } else {
                            val store_past = location_of_picked_before
                            val store_past2 = i
                            var handler = Handler().postDelayed({
                                state_overturn[store_past]--
                                state_overturn[store_past2]--
                                reset(store_past)
                                reset(store_past2)
                            }, 500)
                        }
                        val t: Toast = Toast.makeText(
                            this,
                            "맞춘 횟수:  " + number_of_answer + " 총 시도: " + chance / 2,
                            Toast.LENGTH_SHORT
                        )
                        t.show()
                    }
                }
            })
        }
    }

    fun reset(input:Int){
        if(state_overturn[input]==0)
            show_question_mark(a[input])
    }

    fun show_Clear_box(){
        var textview = findViewById(R.id.clear) as TextView
        textview.setText("Clear!!")
        var score_textview = findViewById(R.id.final_score) as TextView
        score_textview.setText("총 정답:  8/" + " 총 시도: " + chance / 2)
    }

    fun show_Picture(input_image_button: ImageButton?, order : Int, photo_list: ArrayList<PhoneGallery>){
        val image = input_image_button as ImageView
        image.setImageBitmap(photo_list[order].bitmap)
    }

    fun show_question_mark(input_image_button: ImageButton?){
        val imageView = input_image_button as ImageView
        imageView.setImageResource(R.drawable.question_mark)
    }

    fun make_random(total_num:Int ,output:MutableList<Int>): MutableList<Int> {
        if(output.size == 8)
            return output
        val random = Random().nextInt(total_num)
        if(!output.contains(random))
            output.add(random)
        return make_random(total_num, output)
    }

    fun shuffle(input:MutableList<Int>, output:MutableList<Int>): List<Int> {
        val max = input.size

        if(max == 0)
            return output

        val x = Random().nextInt(max)
        output.add(input[x])
        input.removeAt(x)
        return shuffle(input, output)
    }

    fun saveRecord(touchtime: Int){
        GameScoreDatabaseUsgae("card_matching_game",touchtime.toDouble(), 0 ,this).SaveData()
    }
}

