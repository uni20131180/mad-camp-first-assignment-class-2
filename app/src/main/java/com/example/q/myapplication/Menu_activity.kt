package com.example.q.myapplication

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView

class Menu_activity : AppCompatActivity() {

    var REQUIRED_CONNECT = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu)

        var a= window.windowManager.defaultDisplay
        var b = Point()
        a.getSize(b)

        setTitle("게임 모음")
        var button1= findViewById(R.id.App1) as ImageButton
        var button2= findViewById(R.id.App2) as ImageButton
        var button3= findViewById(R.id.App3) as ImageButton
        var button4= findViewById(R.id.App4) as ImageButton
        var button5= findViewById(R.id.App5) as ImageButton
        var button6= findViewById(R.id.App6) as ImageButton

        val view = findViewById(R.id.background) as ImageView

        view.setImageBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.menu_background), b.x, b.y, false))

        button1.setImageBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.card_matchingame_icon), b.x*2/11, b.x/5, false))
        button2.setImageBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.addressgame_icon), b.x*2/11, b.x/5, false))
        button3.setImageBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.slide_puzzle_icon), b.x*2/11, b.x/5, false))
        button4.setImageBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.dodge_icon), b.x*2/11, b.x/5, false))
        button5.setImageBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.battleground_app_icon), b.x*2/11, b.x/5, false))
        button6.setImageBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.snake_icon), b.x*2/11, b.x/5, false))

        button1.setOnClickListener(View.OnClickListener {
            val permissionEX_Storage = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_EXTERNAL_STORAGE)

            if (permissionEX_Storage == PackageManager.PERMISSION_DENIED) {
                REQUIRED_CONNECT = 1
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1)
            }
            else {
                val cardmatchinggameMenu = Intent(this, card_matching_game_menu::class.java)
                startActivity(cardmatchinggameMenu)
            }
        })

        button2.setOnClickListener(View.OnClickListener {

            var permissionaddress = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_CONTACTS)
            if (permissionaddress == PackageManager.PERMISSION_DENIED) {
                REQUIRED_CONNECT = 2
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), 1)
            }
            else {
                val addressmatchinggameActivity = Intent(this, address_matching_game_Activity::class.java)
                startActivity(addressmatchinggameActivity)
            }
        })

        button3.setOnClickListener(View.OnClickListener {
            val permissionEX_Storage = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_EXTERNAL_STORAGE)

            if (permissionEX_Storage == PackageManager.PERMISSION_DENIED) {
                REQUIRED_CONNECT = 3
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1)
            }
            else {
                val slidepuzzlegameActivity = Intent(this, slide_puzzle_game_menu::class.java)
                startActivity(slidepuzzlegameActivity)
            }
        })

        button4.setOnClickListener(View.OnClickListener {
            val permissionEX_Storage = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                val dodgegameActivity = Intent(this, dodge_game_menu::class.java)
                startActivity(dodgegameActivity)
        })

        button5.setOnClickListener(View.OnClickListener {
            val battlegorundActivity = Intent(this, battleground_menu::class.java)
            startActivity(battlegorundActivity)
        })

        button6.setOnClickListener(View.OnClickListener {
            val SnakeActivity = Intent(this, Snake_Game_Menu::class.java)
            startActivity(SnakeActivity)
        })
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        var PERMISSION_REQUEST_CODE = 100
        var check_result = true
        // 모든 퍼미션을 허용했는지 체크합니다.
        if(requestCode == 1){
            for (result in grantResults) {
                println("result : $result")
                if (result != PackageManager.PERMISSION_GRANTED) {
                    check_result = false
                    break
                }
            }
            if (check_result) {
                // 모든 퍼미션을 허용했다면 REQUIRED_CONNECTED 에 따라서 해결한다.
                if(REQUIRED_CONNECT== 1){
                    val cardmatchinggameMenu = Intent(this, card_matching_game_menu::class.java)
                    startActivity(cardmatchinggameMenu)
                }else if(REQUIRED_CONNECT==2){
                    val addressmatchinggameActivity = Intent(this, address_matching_game_Activity::class.java)
                    startActivity(addressmatchinggameActivity)
                }else if(REQUIRED_CONNECT==3){
                    val slidepuzzlegameActivity = Intent(this, slide_puzzle_game_menu::class.java)
                    startActivity(slidepuzzlegameActivity)
                }
            }
        }
    }
}

