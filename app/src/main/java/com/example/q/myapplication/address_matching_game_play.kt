package com.example.q.myapplication
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v7.app.AppCompatActivity
import android.telephony.PhoneNumberUtils
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import java.util.*
import android.view.Gravity



class address_matching_game_play: AppCompatActivity(),View.OnClickListener  {

    private var correctProblem : Int = 0
    private var nowQuestion : Int = 0
    private var clear = false

    class problemIndividual(p : String, c : Boolean){
        var problem : String = p
        var correction : Boolean = c
    }

    class problem(){

        var question : String = ""
        var problemindi: Array<problemIndividual?> = arrayOfNulls<problemIndividual>(3)
        fun makeProblem(set_question:String, set_problem0: problemIndividual, set_problem1: problemIndividual, set_problem2: problemIndividual){
            question = set_question
            problemindi.run {
                set(0, set_problem0)
                set(1, set_problem1)
                set(2, set_problem2)
            }
        }

        fun checkAnswer(int: Int): Boolean {
            return problemindi[int]!!.correction == true
        }

    }
    var problemSet: Array<problem?> = arrayOfNulls<problem>(10)


    override fun onCreate(savedInstanceState: Bundle?) {
        //문제를 만든다
        super.onCreate(savedInstanceState)
        setContentView(R.layout.address_mathing_game_play)
        var phoneList = arrayListOf<PhoneAddress>()
        var uri : Uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        val projection = arrayOf(
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, // 연락처 이름
            ContactsContract.CommonDataKinds.Phone.NUMBER // 연락처
        )
        val sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC"
        val contactCursor = contentResolver.query(uri, projection, null, null, sortOrder)
        var totalSizeOfPhoneList = contactCursor.count

        var random = Random()
        for (i in 0..4){
            // 이름 다섯 개 즉 번호는 열 다섯 개
            var answer = random.nextInt(3)
            var problem = random.nextInt((totalSizeOfPhoneList-2))
            var problemIndividual0 = problemIndividual("",false)
            var problemIndividual1 = problemIndividual("",false)
            var problemIndividual2 = problemIndividual("",false)
            println("totalSize : " + totalSizeOfPhoneList)
            println("Random : " + problem)
            contactCursor.moveToFirst()
            contactCursor.move(problem)
            var question : String = contactCursor.getString(0)
            var question_answer : String = PhoneNumberUtils.formatNumber(contactCursor.getString(1))
            problem = random.nextInt((totalSizeOfPhoneList-2))
            contactCursor.moveToFirst()
            contactCursor.move(problem)
            println("Random : " + problem)
            var question_wrong0 : String = PhoneNumberUtils.formatNumber(contactCursor.getString(1))
            problem = random.nextInt((totalSizeOfPhoneList-2))
            contactCursor.moveToFirst()
            contactCursor.move(problem)
            println("Random : " + problem)
            var question_wrong1 : String = PhoneNumberUtils.formatNumber(contactCursor.getString(1))

            if(answer == 0){
                problemIndividual0!!.correction = true
                problemIndividual0!!.problem = question_answer
                problemIndividual1!!.problem = question_wrong0
                problemIndividual2!!.problem = question_wrong1
            }else if(answer == 1){
                problemIndividual1!!.correction = true
                problemIndividual0!!.problem = question_wrong0
                problemIndividual1!!.problem = question_answer
                problemIndividual2!!.problem = question_wrong1
            }else if(answer == 2){
                problemIndividual2!!.correction = true
                problemIndividual0!!.problem = question_wrong0
                problemIndividual1!!.problem = question_wrong1
                problemIndividual2!!.problem = question_answer
            }
            var pr : problem = problem()
            pr.makeProblem(question,problemIndividual0,problemIndividual1,problemIndividual2)
            problemSet[i] = pr
        }
        for(i in 5..9){
            // 번호 다섯 개 즉 이름은 열 다섯 개
            var answer = random.nextInt(3)
            var problem = random.nextInt((totalSizeOfPhoneList-2))
            var problemIndividual0 = problemIndividual("",false)
            var problemIndividual1 = problemIndividual("",false)
            var problemIndividual2 = problemIndividual("",false)
            contactCursor.moveToFirst()
            contactCursor.move(problem)
            var question : String = PhoneNumberUtils.formatNumber(contactCursor.getString(1))
            var question_answer : String = contactCursor.getString(0)
            problem = random.nextInt((totalSizeOfPhoneList-2))
            contactCursor.moveToFirst()
            contactCursor.move(problem)
            var question_wrong0 : String = contactCursor.getString(0)
            problem = random.nextInt((totalSizeOfPhoneList-2))
            contactCursor.moveToFirst()
            contactCursor.move(problem)
            var question_wrong1 : String = contactCursor.getString(0)

            if(answer == 0){
                problemIndividual0!!.correction = true
                problemIndividual0!!.problem = question_answer
                problemIndividual1!!.problem = question_wrong0
                problemIndividual2!!.problem = question_wrong1
            }else if(answer == 1){
                problemIndividual1!!.correction = true
                problemIndividual0!!.problem = question_wrong0
                problemIndividual1!!.problem = question_answer
                problemIndividual2!!.problem = question_wrong1
            }else if(answer == 2){
                problemIndividual2!!.correction = true
                problemIndividual0!!.problem = question_wrong0
                problemIndividual1!!.problem = question_wrong1
                problemIndividual2!!.problem = question_answer
            }

            var pr : problem = problem()
            pr.makeProblem(question,problemIndividual0,problemIndividual1,problemIndividual2)
            problemSet[i] = pr

        }

        var q = findViewById<TextView>(R.id.addressquestion)
        var a0 = findViewById<Button>(R.id.addressanswer1)
        var a1 = findViewById<Button>(R.id.addressanswer2)
        var a2 = findViewById<Button>(R.id.addressanswer3)

        a0.setOnClickListener(this)
        a1.setOnClickListener(this)
        a2.setOnClickListener(this)

        q.setText(problemSet[nowQuestion]!!.question)
        a0.setText(problemSet[nowQuestion]!!.problemindi[0]!!.problem)
        a1.setText(problemSet[nowQuestion]!!.problemindi[1]!!.problem)
        a2.setText(problemSet[nowQuestion]!!.problemindi[2]!!.problem)
    }

    override fun onClick(v: View?) {
        var selection : Int = 0;
        if(clear){
            finish()
        }else{
            when(v!!.id){
                R.id.addressanswer1 -> {
                    selection = 0
                    println("1번 버튼을 누르셨습니다.")
                }
                R.id.addressanswer2 -> {
                    selection = 1
                    println("2번 버튼을 누르셨습니다.")
                }
                R.id.addressanswer3 -> {
                    selection = 2
                    println("3번 버튼을 누르셨습니다.")
                }
            }

            if(problemSet[nowQuestion]!!.problemindi[selection]!!.correction == true){
                correctProblem+=1
                var toast = Toast.makeText(this, ("정답! 현재까지 맞춘 갯수 " + correctProblem) , Toast.LENGTH_SHORT)
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, -30)
                toast.show()
            }else{
                var toast = Toast.makeText(this, ("오답... 현재까지 맞춘 갯수 " + correctProblem) , Toast.LENGTH_SHORT)
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, -30)
                toast.show()
            }
            nowQuestion+=1
            if(nowQuestion < 10){
                var q = findViewById<TextView>(R.id.addressquestion)
                var a0 = findViewById<Button>(R.id.addressanswer1)
                var a1 = findViewById<Button>(R.id.addressanswer2)
                var a2 = findViewById<Button>(R.id.addressanswer3)

                a0.setOnClickListener(this)
                a1.setOnClickListener(this)
                a2.setOnClickListener(this)

                q.setText(problemSet[nowQuestion]!!.question)
                a0.setText(problemSet[nowQuestion]!!.problemindi[0]!!.problem)
                a1.setText(problemSet[nowQuestion]!!.problemindi[1]!!.problem)
                a2.setText(problemSet[nowQuestion]!!.problemindi[2]!!.problem)
            }else{
                GameScoreDatabaseUsgae("addressmatching",correctProblem.toDouble(), 0 ,this).SaveData()
                clear = true
            }
        }

    }

}