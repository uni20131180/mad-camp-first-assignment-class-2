package com.example.q.myapplication


import android.app.*
import android.content.*
import android.graphics.*
import android.os.*
import android.view.*
import android.view.MotionEvent
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.widget.TextView
import android.widget.Toast
import java.util.*


class battleground_activity : Activity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // 전체 화면 사용하기
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(GameView(this))
    }

    //-----------------------------------
    //       Game View
    //-----------------------------------
    internal inner class GameView//-----------------------------------
    //   Constructor - 게임 초기화
    //-----------------------------------
        (context: Context) : View(context) {                          // 화면의 폭과 높이
        var dx: Float = 0.toFloat()
        var dy: Float = 0.toFloat()                                        // 캐릭터가 이동할 방향과 거리
        var character : BGcharacter? = null
        var cw= 0
        var ch= 0               // 캐릭터의 폭과 높이
        var backimage = BitmapFactory.decodeResource(resources, R.drawable.battleground_map)!!
        var characterimage = BitmapFactory.decodeResource(resources, R.drawable.battleground_character)!!
        var enemyimage = BitmapFactory.decodeResource(resources, R.drawable.fortnite_character2)!!
        var bossimage = BitmapFactory.decodeResource(resources, R.drawable.fortnite_character)!!
        var bullet1image = BitmapFactory.decodeResource(resources, R.drawable.bullets1)!!
        var bullet2image = BitmapFactory.decodeResource(resources, R.drawable.bullets2)!!
        var x1: Float = 0.toFloat()
        var y1: Float = 0.toFloat()
        var x2: Float = 0.toFloat()
        var y2: Float = 0.toFloat()                             // Down, Up
        var canRun = true
        var count = 0
        var index = 0
        var real_width = 0
        var real_height = 0
        var enemy_list = mutableListOf<BGcharacter>()
        var movex = mutableListOf<Int>()
        var movey = mutableListOf<Int>()
        var number_of_killed = 0


        //------------------------------------
        //      Timer Handler
        //------------------------------------
        var mHandler: Handler = object : Handler() {               // 타이머로 사용할 Handler
            override fun handleMessage(msg: Message) {
                if (canRun == true) {
                    invalidate()                                              // onDraw() 다시 실행
                    this.sendEmptyMessageDelayed(0, 10) // 10/1000초마다 실행
                } else{
                    saveRecord(number_of_killed)
                    var handler = Handler().postDelayed({
                        finish()
                    }, 1000000000)
                }
            }
        } // Handler

        init {
            val display = (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
            real_width = display.width          // 화면의 가로폭
            real_height = display.height        // 화면의 세로폭

            character = BGcharacter(characterimage, 0, real_width/2.toFloat() ,real_height.toFloat() )
            cw= character!!.myimage_bitmap.getWidth() / 2
            ch= character!!.myimage_bitmap.getHeight()                  // 캐릭터의 폭과 높이
            character!!.my_x = character!!.my_x - cw
            character!!.my_y = character!!.my_y - ch

            backimage = Bitmap.createScaledBitmap(backimage, real_width, real_height,false)

            mHandler.sendEmptyMessageDelayed(0, 10)
        }

        //-----------------------------------
        //       실제 그림을 그리는 부분
        //-----------------------------------
        public override fun onDraw(canvas: Canvas) {
            if(character!!.HP <=0){
                canRun = false
            }
            canvas.drawBitmap(backimage, 0.toFloat(), 0.toFloat(), null)
            make_enemy(700, 100)
            make_enemy(1000, 600)
            make_enemy(1500, 1100)
            make_enemy(2000, 1600)

            character!!.draw(canvas)
            character!!.shoot_above()
            var number_of_dead = 0
            for(i in 0 until enemy_list.size) {
                val k = i-number_of_dead
                var BGenemy = enemy_list[k]
                var random_constant = Random().nextInt(100)
                if(random_constant%50==4)
                    movex[k] = -movex[k]
                if(random_constant%50==4)
                    movey[k] = -movey[k]

                if(count%2==0){
                    var cx = BGenemy.my_x + movex[k]
                    if(cx <= real_width-BGenemy.myimage_bitmap.width && cx >= 0)
                        BGenemy.my_x = cx
                    else
                        movex[k] = -movex[k]

                    var cy = BGenemy.my_y + movey[k]
                    if(cy >= 0 && cy <= real_height/2 - BGenemy.myimage_bitmap.height)
                        BGenemy.my_y = cy
                    else
                        movey[k] = -movey[k]
                }
                if(count%50 == 0)
                    BGenemy.shoot_enemy(bullet2image)

                BGenemy.bullet_check(character!!.my_bullets)
                character!!.bullet_check((BGenemy.my_bullets))
                if(BGenemy.HP <=0){
                    enemy_list.removeAt(k)
                    number_of_dead++
                    number_of_killed++
                }
                else{
                    BGenemy.draw(canvas)
                    BGenemy.shoot_below(canvas)
                    BGenemy.draw_bullet(canvas)
                    enemy_list[k]=BGenemy
                }
            }

            if(count%50 == 0)
                character!!.shoot(bullet1image)
            character!!.draw_bullet(canvas)
            count++


        } // onDraw 끝

        private fun make_enemy(cycle:Int, start_cycle:Int){
            if(count%cycle == start_cycle){
                var random_start_x = Random().nextInt(real_width-bossimage.width/10)
                var random_start_y: Int = Random().nextInt(real_height/2-bossimage.height/10)
                if(cycle==1500)
                    enemy_list.add(BGcharacter(bossimage, 4, random_start_x.toFloat(), random_start_y.toFloat()))
                else
                    enemy_list.add(BGcharacter(enemyimage, 2, random_start_x.toFloat(), random_start_y.toFloat()))
                movex.add(10)
                movey.add(10)
            }
        }
        //------------------------------------
        //      onTouchEvent
        //------------------------------------
        override fun onTouchEvent(event: MotionEvent): Boolean {
            if (event.action == MotionEvent.ACTION_DOWN) {
                x1 = event.x                   // 버튼을 누른 위치
                y1 = event.y
            } else if (event.action == MotionEvent.ACTION_MOVE) {
                x2 = event.x                   // 버튼을 이동한 후 손을 뗀 위치
                y2 = event.y
                dx = (x2 - x1) / 33                // 버튼의 거리
                dy = (y2 - y1) / 33

                var cx = character!!.my_x + dx
                if(0 < dx && cx <= real_width.toFloat() - character!!.myimage_bitmap.width)
                    character!!.my_x = cx
                else if(0 > dx && cx>0)
                    character!!.my_x = cx

                var cy = character!!.my_y + dy
                if(0 < dy && cy <= real_height.toFloat() - character!!.myimage_bitmap.height)
                    character!!.my_y = cy
                else if(0 > dy && cy>0)
                    character!!.my_y = cy
            }
            return true
        } // onTouchEvent
    } // GameView 끝



    fun saveRecord(touchtime: Int){
        GameScoreDatabaseUsgae("BattleGround",touchtime.toDouble(), 0 ,this).SaveData()
    }
} // 프로그램 끝